﻿namespace Blazor.RecipeBook.Models.LearnBlazorModels
{
    public class DemoProduct
    {
        public bool IsActive { get; set; }

        public double Price { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public List<DemoProductProperties> ProductProperties { get; set; }
    }
}