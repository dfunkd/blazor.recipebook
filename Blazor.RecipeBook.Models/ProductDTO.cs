﻿using Blazor.RecipeBook.DataAccess;
using System.ComponentModel.DataAnnotations;

namespace Blazor.RecipeBook.Models
{
    public class ProductDTO
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please select a category.")]
        public int CategoryId { get; set; }
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public string Color { get; set; }
        public string ImageUrl { get; set; }

        public bool ShopFavorites { get; set; }
        public bool CustomerFavorites { get; set; }

        public Category Category { get; set; }
    }
}
