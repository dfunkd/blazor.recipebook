﻿using System.ComponentModel.DataAnnotations;

namespace Blazor.RecipeBook.DataAccess
{
    public class Measurement
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Abbreviation { get; set; }
        [Required]
        public string Name { get; set; }
    }
}