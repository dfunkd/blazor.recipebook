﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blazor.RecipeBook.DataAccess
{
    public class Ingredient
    {
        [Key]
        public int Id { get; set; }
        public int MeasurementId { get; set; }
        public int RecipeId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Quantity { get; set; }

        [ForeignKey("MeasurementId")]
        public Measurement Measurement { get; set; }

        [ForeignKey("RecipeId")]
        public Recipe Recipe { get; set; }
    }
}