﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blazor.RecipeBook.DataAccess
{
    public class Product
    {
        public int CategoryId { get; set; }
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public string ImageUrl { get; set; }

        public bool ShopFavorites { get; set; }
        public bool CustomerFavorites { get; set; }

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
    }
}