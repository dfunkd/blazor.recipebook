﻿using Microsoft.EntityFrameworkCore;

namespace Blazor.RecipeBook.DataAccess.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        #region DbSets
        //add-migration AddCategoryToDatabase
        public DbSet<Category> Categories { get; set; }
        //add-migration AddProductToDatabase
        public DbSet<Product> Products { get; set; }
        //update-database
        #endregion
    }
}