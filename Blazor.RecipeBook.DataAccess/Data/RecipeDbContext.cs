﻿using Microsoft.EntityFrameworkCore;

namespace Blazor.RecipeBook.DataAccess.Data
{
    public class RecipeDbContext : DbContext
    {
        public RecipeDbContext(DbContextOptions<RecipeDbContext> options)
            : base(options)
        {

        }

        #region DbSets
        //add-migration AddIngredientToDatabase
        public DbSet<Ingredient> Ingredients { get; set; }
        //add-migration AddMeasurementToDatabase
        public DbSet<Measurement> Measurements { get; set; }
        //add-migration AddRecipeToDatabase
        public DbSet<Recipe> Recipes { get; set; }
        //update-database
        #endregion
    }
}