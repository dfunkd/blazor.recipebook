﻿using System.ComponentModel.DataAnnotations;

namespace Blazor.RecipeBook.DataAccess
{
    public class Recipe
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int IsGood { get; set; }
        public int CookTime { get; set; }
        public int PrepTime { get; set; }
        public int Servings { get; set; }

        public DateTime DateCreated { get; set; }

        [Required]
        public string Directions { get; set; }
        [Required]
        public string Name { get; set; }
        public string Notes { get; set; }
        public string Tips { get; set; }
        public string Url { get; set; }

        public ICollection<Ingredient> Ingredients { get; set; }
    }
}