﻿using Blazor.RecipeBook.Models;
using Microsoft.JSInterop;

namespace Blazor.RecipeBook.Server.Pages.Category
{
    public partial class CategoryList
    {
        private IEnumerable<CategoryDTO> Categories { get; set; } = new List<CategoryDTO>();
        public bool IsLoading { get; set; }
        private int DeleteCategoryId { get; set; } = 0;

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
                await LoadCategories();
        }

        private void HandleDelete(int id)
        {
            DeleteCategoryId = id;
            _jsRuntime.InvokeVoidAsync("ShowDeleteConfirmationModal");
        }

        private async Task LoadCategories()
        {
            IsLoading = true;
            StateHasChanged();
            Categories = await _categoryRepository.GetAll();
            IsLoading = false;
            StateHasChanged();
        }

        public async Task OnConfirmDeleteClick(bool isConfirmed)
        {
            IsLoading = true;
            if (isConfirmed && DeleteCategoryId != 0)
            {
                await _categoryRepository.Delete(DeleteCategoryId);
                await LoadCategories();
                await _jsRuntime.InvokeVoidAsync("HideDeleteConfirmationModal");
            }
            IsLoading = false;
        }
    }
}