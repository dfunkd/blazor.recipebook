﻿using Blazor.RecipeBook.Server.Helpers;
using Microsoft.JSInterop;

namespace Blazor.RecipeBook.Server.Pages.LearnBlazor
{
    public partial class BlazorJs
    {
        private string confirmMessage = "Are you sure you want to click?";
        private bool confirmResult { get; set; }

        private async Task TestConfirmBox()
        {
            confirmResult = await _JsRuntime.InvokeAsync<bool>("confirm", confirmMessage);
        }
        private async Task ToastrSuccess()
        {
            await _JsRuntime.ToastrSuccess("Operation Successful");
        }
        private async Task ToastrFailure()
        {
            await _JsRuntime.ToastrError("Operation Failed");
        }
        private async Task SweetAlertSuccess()
        {
            await _JsRuntime.InvokeVoidAsync("ShowSweetAlert", "success", "Task completed successfully!");
        }
        private async Task SweetAlertFailure()
        {
            await _JsRuntime.InvokeVoidAsync("ShowSweetAlert", "error", "Task failed!");
        }
    }
}