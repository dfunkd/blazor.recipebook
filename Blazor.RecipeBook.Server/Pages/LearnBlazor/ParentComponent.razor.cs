﻿using Blazor.RecipeBook.Server.Shared.LearnBlazorComponents;

namespace Blazor.RecipeBook.Server.Pages.LearnBlazor
{
    public partial class ParentComponent
    {
        private _ChildComponent childComp;

        public string LuckyNumber = "7";
        public string MessageForGrandchild { get; set; } = "This message is from your grandparent (ParentComponent)";
        public string MessageText = string.Empty;

        private void ShowMessage()
        {
            MessageText = "Button clicked from Child Component";
        }

        public Dictionary<string, object> InputAttributesFromParent { get; set; } = new()
        {
            { "required", "required" },
            { "placeholder", "Enter Name From Parent" },
            { "maxLength", 10 }
        };
    }
}