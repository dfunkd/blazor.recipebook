﻿using Blazor.RecipeBook.Models.LearnBlazorModels;

namespace Blazor.RecipeBook.Server.Pages.LearnBlazor
{
    public partial class ProductDemo
    {
        private int selectedFavoritesCount { get; set; } = 0;
        private string selectedProperty = string.Empty;
        private string lastSelectedProductName { get; set; }
        public DemoProduct Product { get; set; }
        public List<DemoProduct> Products = new();

        protected override Task OnInitializedAsync()
        {
            Product = new()
            {
                Id = 1,
                IsActive = true,
                Name = "Rose Candle",
                Price = 10.99,
                ProductProperties = new List<DemoProductProperties>()
                {
                    new() { Id = 1, Key = "Color", Value = "Black"},
                    new() { Id = 2, Key = "Flavor", Value = "Rose Jasmine" },
                    new() { Id = 3, Key = "Size", Value = "20oz" }
                }
            };

            Products.Add(new()
            {
                Id = 1,
                IsActive = false,
                Name = "Midnight Blaze",
                Price = 10.99,
                ProductProperties = new()
                {
                    new() { Id = 1, Key = "Flavor", Value = "Rose" },
                    new() { Id = 2, Key = "Size", Value = "20oz" },
                    new() { Id = 3, Key = "Color", Value = "Purple" },
                }
            });
            Products.Add(new()
            {
                Id = 2,
                IsActive = true,
                Name = "Blossom Lily",
                Price = 13.99,
                ProductProperties = new()
                {
                    new() { Id = 1, Key = "Flavor", Value = "Lily" },
                    new() { Id = 2, Key = "Size", Value = "18oz" },
                    new() { Id = 3, Key = "Color", Value = "White" },
                }
            });

            return base.OnInitializedAsync();
        }

        protected void FavritesCountUpdate(bool isSelected)
        {
            if (isSelected)
                selectedFavoritesCount++;
            else
                selectedFavoritesCount--;
        }

        protected void SelectedProductUpdate(string productName)
        {
            lastSelectedProductName = productName;
        }
    }
}