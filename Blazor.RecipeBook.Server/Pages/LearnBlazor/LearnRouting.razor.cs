﻿using Microsoft.AspNetCore.Components;
using System.Web;

namespace Blazor.RecipeBook.Server.Pages.LearnBlazor
{
    public partial class LearnRouting
    {
        [Parameter]
        public int Parameter1 { get; set; }
        [Parameter]
        public string Parameter2 { get; set; }

        public string Param1 { get; set; }
        public string Param2 { get; set; }

        private void LoadParameters()
        {
            Uri absoluteUri = new(_NavigationManager.Uri);
            var queryParams = HttpUtility.ParseQueryString(absoluteUri.Query);
            Param1 = queryParams["Param1"];
            Param2 = queryParams["Param2"];
        }

        private void NavigateToQueryParam()
        {
            _NavigationManager.NavigateTo("learnrouting?Param1=Recipes&Param2=Blazor");
        }
    }
}