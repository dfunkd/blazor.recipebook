﻿namespace Blazor.RecipeBook.Server.Pages.LearnBlazor
{
    public partial class LifeCycle
    {
        private int currentCount = 0;
        private List<string> EventType = new();
        private int Count { get; set; } = 5;

        #region On Initialize
        protected override void OnInitialized()
        {
            EventType.Add("OnInitialized is called");
        }
        protected override async Task OnInitializedAsync()
        {
            EventType.Add("OnInitializedAsync is called");
            await Task.Delay(1000);
        }
        #endregion

        #region OnParametersSet
        protected override void OnParametersSet()
        {
            EventType.Add("OnParameterSet is called");
        }
        protected override async Task OnParametersSetAsync()
        {
            EventType.Add("OnParametersSetAsync is called");
            await Task.Delay(1000);
        }
        #endregion

        #region OnAfterRender
        protected override void OnAfterRender(bool firstRender)
        {
            currentCount = firstRender ? 111 : 999;
            EventType.Add("OnAfterRender is called");
        }
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            EventType.Add("OnAfterRenderAsync is called");
            await Task.Delay(1000);
        }
        #endregion

        protected override bool ShouldRender()
        {
            EventType.Add("OnShouldRender is called");
            return true;
        }

        private void StateHStartCountdown()
        {
            Timer timer = new(TimeCallback, null, 1000, 1000);
        }
        private void TimeCallback(object state)
        {
            if (Count > 0)
            {
                Count--;
                InvokeAsync(StateHasChanged);
            }
        }

        private void IncrementCount()
        {
            currentCount++;
        }
    }
}