﻿using Blazor.RecipeBook.Models;
using Blazor.RecipeBook.Server.Helpers;
using Microsoft.JSInterop;

namespace Blazor.RecipeBook.Server.Pages.Product
{
    public partial class ProductList
    {
        private IEnumerable<ProductDTO> Products { get; set; } = new List<ProductDTO>();
        public bool IsLoading { get; set; }
        private int DeleteProductId { get; set; } = 0;

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
                await LoadProducts();
        }

        private void HandleDelete(int id)
        {
            DeleteProductId = id;
            _jsRuntime.InvokeVoidAsync("ShowDeleteConfirmationModal");
        }

        private async Task LoadProducts()
        {
            IsLoading = true;
            StateHasChanged();
            Products = await _productRepository.GetAll();
            IsLoading = false;
            StateHasChanged();
        }

        public async Task OnConfirmDeleteClick(bool isConfirmed)
        {
            IsLoading = true;
            if (isConfirmed && DeleteProductId != 0)
            {
                ProductDTO product = await _productRepository.Get(DeleteProductId);
                if (!product.ImageUrl.Contains("default.jpg"))
                {
                    _fileUpload.DeleteFile(product.ImageUrl);
                }
                await _productRepository.Delete(DeleteProductId);
                await _jsRuntime.ToastrSuccess("Product deleted successfully.");
                await LoadProducts();
                await _jsRuntime.InvokeVoidAsync("HideDeleteConfirmationModal");
            }
            IsLoading = false;
        }
    }
}