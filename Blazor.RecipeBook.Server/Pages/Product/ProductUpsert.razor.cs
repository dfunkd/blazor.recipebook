﻿using Blazor.RecipeBook.Models;
using Blazor.RecipeBook.Server.Helpers;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace Blazor.RecipeBook.Server.Pages.Product
{
    public partial class ProductUpsert
    {
        [Parameter]
        public int Id { get; set; }

        private ProductDTO Product { get; set; } = new()
        {
            ImageUrl = "/images/default.jpg"
        };
        private IEnumerable<CategoryDTO> Categories { get; set; } = new List<CategoryDTO>();
        private string Title { get; set; } = "Create";
        public string OldImageUrl { get; set; }
        public bool IsLoading { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
                await LoadProduct();
        }

        private async Task LoadProduct()
        {
            IsLoading = true;
            StateHasChanged();
            Categories = await _categoryRepository.GetAll();
            if (Id != 0)
            {
                Title = "Update";
                Product = await _productRepository.Get(Id);
                OldImageUrl = Product.ImageUrl;
            }
            IsLoading = false;
            StateHasChanged();
        }

        private async Task UpsertProduct()
        {
            if (Product.Id == 0)
            {
                await _productRepository.Create(Product);
                await _jsRuntime.ToastrSuccess("Product created successfully.");
            }
            else
            {
                if (OldImageUrl != Product.ImageUrl)
                    _fileUpload.DeleteFile(OldImageUrl);
                await _productRepository.Update(Product);
                await _jsRuntime.ToastrSuccess("Product updated successfully.");
            }

            _navigationManager.NavigateTo("product");
        }

        private async Task HandleUpload(InputFileChangeEventArgs e)
        {
            IsLoading = true;
            try
            {
                if (e.GetMultipleFiles().Count > 0)
                {
                    foreach (var file in e.GetMultipleFiles())
                    {
                        FileInfo fileInfo = new(file.Name);
                        if (fileInfo.Extension.ToLower() == ".jpg" || fileInfo.Extension.ToLower() == ".png" || fileInfo.Extension.ToLower() == ".jpeg")
                        {
                            Product.ImageUrl = await _fileUpload.UploadFile(file);
                        }
                        else
                        {
                            await _jsRuntime.ToastrError("Please select .jpg / .jpeg / .png file only.");
                            return;
                        }
                    }
                }
                IsLoading = false;
            }
            catch (Exception ex)
            {
                await _jsRuntime.ToastrError(ex.Message);
            }
        }
    }
}