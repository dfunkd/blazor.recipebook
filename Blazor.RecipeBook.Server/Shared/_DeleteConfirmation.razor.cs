﻿using Microsoft.AspNetCore.Components;

namespace Blazor.RecipeBook.Server.Shared
{
    public partial class _DeleteConfirmation
    {
        [Parameter]
        public bool IsParentComponentProcessing { get; set; }

        [Parameter]
        public EventCallback<bool> ConfirmationChanged { get; set; }

        private async Task OnConfirmationDelete(bool value)
        {
            if (value)
            {
                await ConfirmationChanged.InvokeAsync(value);
            }
        }
    }
}