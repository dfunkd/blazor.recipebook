﻿using Blazor.RecipeBook.Server.Helpers;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace Blazor.RecipeBook.Server.Shared.LearnBlazorComponents
{
    public partial class _ChildComponent
    {
        [Parameter]
        public string Title { get; set; }
        [Parameter]
        public RenderFragment ChildContent { get; set; }
        [Parameter]
        public RenderFragment DangerChildContent { get; set; }
        [Parameter]
        public EventCallback OnButtonClick { get; set; }

        public async Task ToastrSuccess()
        {
            await _JsRuntime.ToastrSuccess("Operation Successful");
        }
        public async Task ToastrFailure()
        {
            await _JsRuntime.ToastrError("Operation Failed");
        }
        public async Task SweetAlertSuccess()
        {
            await _JsRuntime.InvokeVoidAsync("ShowSweetAlert", "success", "Task completed successfully!");
        }
        public async Task SweetAlertFailure()
        {
            await _JsRuntime.InvokeVoidAsync("ShowSweetAlert", "error", "Task failed!");
        }
    }
}