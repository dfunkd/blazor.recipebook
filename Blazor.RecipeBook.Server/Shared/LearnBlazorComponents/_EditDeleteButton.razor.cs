﻿using Microsoft.AspNetCore.Components;

namespace Blazor.RecipeBook.Server.Shared.LearnBlazorComponents
{
    public partial class _EditDeleteButton
    {
        [Parameter]
        public bool IsActive { get; set; }
    }
}