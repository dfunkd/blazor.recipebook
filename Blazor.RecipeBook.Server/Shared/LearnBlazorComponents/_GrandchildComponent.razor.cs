﻿using Microsoft.AspNetCore.Components;

namespace Blazor.RecipeBook.Server.Shared.LearnBlazorComponents
{
    public partial class _GrandchildComponent
    {
        [CascadingParameter(Name = "luckyNumber")]
        public int LuckyNumberForGrandchild { get; set; }
        [CascadingParameter(Name = "message")]
        public string MessageForGrandchild { get; set; }
    }
}