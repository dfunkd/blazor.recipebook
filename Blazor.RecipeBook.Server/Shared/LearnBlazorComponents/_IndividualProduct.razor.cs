﻿using Blazor.RecipeBook.Models.LearnBlazorModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Blazor.RecipeBook.Server.Shared.LearnBlazorComponents
{
    public partial class _IndividualProduct
    {
        [Parameter]
        public DemoProduct Product { get; set; }
        [Parameter]
        public EventCallback<bool> OnFavoritesUpdated { get; set; }
        [Parameter]
        public EventCallback<string> OnLastSelectedProductChange { get; set; }
        [Parameter]
        public RenderFragment FirstFragment { get; set; }
        [Parameter]
        public RenderFragment SecondFragment { get; set; }

        public async Task FavoritesUpdated(ChangeEventArgs e)
        {
            await OnFavoritesUpdated.InvokeAsync((bool)e.Value);
        }

        public async Task LastSelectedProduct(MouseEventArgs e, string name)
        {
            await OnLastSelectedProductChange.InvokeAsync(name);
        }
    }
}