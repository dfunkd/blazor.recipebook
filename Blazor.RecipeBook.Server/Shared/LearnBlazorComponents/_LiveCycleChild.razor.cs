﻿using Microsoft.AspNetCore.Components;

namespace Blazor.RecipeBook.Server.Shared.LearnBlazorComponents
{
    public partial class _LiveCycleChild
    {
        private List<string> EventType = new();

        #region Parameters
        [Parameter]
        public int CountValue { get; set; }
        #endregion

        #region On Initialize
        protected override void OnInitialized()
        {
            EventType.Add("Child - OnInitialized is called");
        }
        //protected override async Task OnParametersSetAsync()
        //{
        //    EventType.Add("OnInitializedAsync is called");
        //    await Task.Delay(1000);
        //}
        #endregion

        #region OnParameterSet
        protected override void OnParametersSet()
        {
            EventType.Add("Child - OnParameterSet is called");
        }
        //protected override async Task OnParametersSetAsync()
        //{
        //    EventType.Add("OnParameterSetAsync is called");
        //    await Task.Delay(1000);
        //}
        #endregion

        #region OnAfterRender
        protected override void OnAfterRender(bool firstRender)
        {
            EventType.Add("OnAfterRender is called");
        }
        //protected override async Task OnAfterRenderAsync(bool firstRender)
        //{
        //    EventType.Add("OnAfterRenderAsync is called");
        //    await Task.Delay(1000);
        //}
        #endregion
    }
}