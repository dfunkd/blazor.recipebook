﻿using Microsoft.AspNetCore.Components.Forms;

namespace Blazor.RecipeBook.Server.Service.IService
{
    public interface IFileUpload
    {
        bool DeleteFile(string filePath);
        Task<string> UploadFile(IBrowserFile file);
    }
}