﻿using AutoMapper;
using Blazor.RecipeBook.Business.Repository.IRepository;
using Blazor.RecipeBook.DataAccess;
using Blazor.RecipeBook.DataAccess.Data;
using Blazor.RecipeBook.Models;
using Microsoft.EntityFrameworkCore;

namespace Blazor.RecipeBook.Business.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;

        public ProductRepository(ApplicationDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<ProductDTO> Create(ProductDTO ProductDTO)
        {
            Product Product = _mapper.Map<ProductDTO, Product>(ProductDTO);

            var addedProduct = _db.Products.Add(Product);
            await _db.SaveChangesAsync();

            return _mapper.Map<Product, ProductDTO>(addedProduct.Entity);
        }

        public async Task<int> Delete(int id)
        {
            var Product = await _db.Products.FirstOrDefaultAsync(f => f.Id == id);
            if (Product != null)
            {
                _db.Products.Remove(Product);
                await _db.SaveChangesAsync();
            }
            return 0;
        }

        public async Task<ProductDTO> Get(int id)
        {
            var Product = await _db.Products.Include(i => i.Category).FirstOrDefaultAsync(f => f.Id == id);
            if (Product != null)
                return _mapper.Map<Product, ProductDTO>(Product);
            return new();
        }

        public async Task<IEnumerable<ProductDTO>> GetAll() => _mapper.Map<IEnumerable<Product>, IEnumerable<ProductDTO>>(_db.Products.Include(i => i.Category));

        public async Task<ProductDTO> Update(ProductDTO ProductDTO)
        {
            var Product = await _db.Products.FirstOrDefaultAsync(f => f.Id == ProductDTO.Id);

            if (Product != null)
            {
                Product.CategoryId = ProductDTO.CategoryId;
                Product.Color = ProductDTO.Color;
                Product.CustomerFavorites = ProductDTO.CustomerFavorites;
                Product.Description = ProductDTO.Description;
                Product.ImageUrl = ProductDTO.ImageUrl;
                Product.Name = ProductDTO.Name;
                Product.ShopFavorites = ProductDTO.ShopFavorites;

                _db.Products.Update(Product);
                await _db.SaveChangesAsync();
                return _mapper.Map<Product, ProductDTO>(Product);
            }
            return ProductDTO;
        }
    }
}