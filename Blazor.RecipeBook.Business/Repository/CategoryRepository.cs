﻿using AutoMapper;
using Blazor.RecipeBook.Business.Repository.IRepository;
using Blazor.RecipeBook.DataAccess;
using Blazor.RecipeBook.DataAccess.Data;
using Blazor.RecipeBook.Models;
using Microsoft.EntityFrameworkCore;

namespace Blazor.RecipeBook.Business.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;

        public CategoryRepository(ApplicationDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<CategoryDTO> Create(CategoryDTO categoryDTO)
        {
            Category category = _mapper.Map<CategoryDTO, Category>(categoryDTO);
            category.CreatedDate = DateTime.Now;

            var addedCategory = _db.Categories.Add(category);
            await _db.SaveChangesAsync();

            return _mapper.Map<Category, CategoryDTO>(addedCategory.Entity);
        }

        public async Task<int> Delete(int id)
        {
            var category = await _db.Categories.FirstOrDefaultAsync(f => f.Id == id);
            if (category != null)
            {
                _db.Categories.Remove(category);
                await _db.SaveChangesAsync();
            }
            return 0;
        }

        public async Task<CategoryDTO> Get(int id)
        {
            var category = await _db.Categories.FirstOrDefaultAsync(f => f.Id == id);
            if (category != null)
                return _mapper.Map<Category, CategoryDTO>(category);
            return new();
        }

        public async Task<IEnumerable<CategoryDTO>> GetAll() => _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDTO>>(_db.Categories);

        public async Task<CategoryDTO> Update(CategoryDTO categoryDTO)
        {
            var category = await _db.Categories.FirstOrDefaultAsync(f => f.Id == categoryDTO.Id);

            if (category != null)
            {
                category.Name = categoryDTO.Name;
                _db.Categories.Update(category);
                await _db.SaveChangesAsync();
                return _mapper.Map<Category, CategoryDTO>(category);
            }
            return categoryDTO;
        }
    }
}