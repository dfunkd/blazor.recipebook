﻿using Blazor.RecipeBook.Models;

namespace Blazor.RecipeBook.Business.Repository.IRepository
{
    public interface IProductRepository
    {
        public Task<ProductDTO> Create(ProductDTO ProductDTO);
        public Task<int> Delete(int id);
        public Task<ProductDTO> Get(int id);
        public Task<IEnumerable<ProductDTO>> GetAll();
        public Task<ProductDTO> Update(ProductDTO ProductDTO);
    }
}