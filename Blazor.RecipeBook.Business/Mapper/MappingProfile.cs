﻿using AutoMapper;
using Blazor.RecipeBook.DataAccess;
using Blazor.RecipeBook.Models;

namespace Blazor.RecipeBook.Business.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Category, CategoryDTO>().ReverseMap();
            CreateMap<Product, ProductDTO>().ReverseMap();
        }
    }
}